---
marp: true
theme: my-theme
headingDivider: 4
size: 16:9
paginate: true
---

# タイトル<!-- omit in toc -->

<!-- _class: title -->

## セクションタイトル

<!-- _class: section-title -->

### 見出し1

<!-- _class: subsection -->

#### 見出し1-1

<div class="left">

1. aaa
2. bbb

</div>

<div class="right">

| col1 | col2 | col3 |
| ---- | ---- | ---- |
| 1    | a    | あ   |
| 2    | b    | い   |
| 3    | c    | う   |

</div>

#### 見出し1-2

<div class="left">
    <div class="mermaid" style="text-align: center;">
        flowchart LR
            A[作業A]
            B[作業B]
            C[作業C]
            A --> B --> C
    </div>
</div>

<div class="right">

- aaa
- bbb

</div>

#### 見出し1-3

<div class="left">

<!-- https://kroki.io/ -->
![center w:500](https://kroki.io/blockdiag/svg/eNpdzDEKQjEQhOHeU4zpPYFoYesRxGJ9bwghMSsbUYJ4d10UCZbDfPynolOek0Q8FsDeNCestoisNLmy-Qg7R3Blcm5hPcr0ITdaB6X15fv-_YdJixo2CNHI2lmK3sPRA__RwV5SzV80ZAegJjXSyfMFptc71w==)

</div>

<div class="right">

- aaaa

</div>

